<!-- This file consists of notes for Apache Maven-->
# Apache Maven

![Apache Maven Logo](https://maven.apache.org/images/maven-logo-black-on-white.png)

## Introduction

### What is Maven?

Apache Maven is a tool used in software development to make project management and comprehension easier for the developer. It provides several key functionalities, as follows:-

1. Simplifies the process of building the project
    > Maven allows the developer to be shielded from the many details of project building while still maintaining a need to know the its underlying mechanisms.

1. Introduces uniformity in the build system
    > Through the use of Maven's project object model (POM) and a plugin set, Maven allows for familiarization across any and all projects built with it.

### Maven Lifecycles
Building Java projects using Maven involves going through lifecycles and phases. Each lifecycle is split into phases. In layman terms, 'lifecycles' refer to the jobs carried out by Maven and 'phases' refer to the steps taken to carry them out.

When a lifecycle phase is invoked using the command ```mvn``` followed by the phase, all phases preceding the invoked phase, as well as the invoked phase itself, will be carried out one after another. It is important to note that the phases in a lifecycle in itself do not carry out any operations. The phases invoke plugins in order to carry out specific tasks. Phases are just sets of predefined steps which Maven calls on in sequence.

Besides the options field, a ```mvn``` command accepts parameters in only two other fields: **goal** and **phase**. Each phase is a sequence of goals, and each goal is responsible for a specific task. A Maven plugin, also known as Mojo - Maven Plain Old Java Object, is a collection of one or more goals which do some task or job. Essentially, when a lifecycle phases is called through the command line, Maven binds plugin goals to their respective lifecycle phases.

Some commonly used plugins are:-

|Plugin|Description|
|---|---|
|```site```|Generates a site for the current project.|
|```assembly```|Build a distribution of sources and/or binaries.|
|```release```|Releases the current project by updating the POM and tagging in the SCM.|

Maven has three distinct lifecycles: **default**, **clean**, and **site**.

#### Clean
The ```mvn clean``` command cleans the project by deleting the build directory called ```./target``` and its contents, which is generated during the previous build. The clean lifecycle consists of three phases - **pre-clean**, **clean**, and **post-clean**.

#### Default
The default lifecycle is widely considered as the main lifecycle because it is responsible for the deployment of the project. It contains the most number of phases among all the lifecycles, for a grand total of 21 phases.

|Phase|Description|
|---|---|
|```validate```|validate the project is correct and all necessary information is available.|
|```initialize```|initialize build state, e.g. set properties or create directories.|
|```generate-sources```|generate any source code for inclusion in compilation.|
|```process-sources```|process the source code, for example to filter any values.|
|```generate-resources```|generate resources for inclusion in the package.|
|```process-resources```|	copy and process the resources into the destination directory, ready for packaging.|
|```compile```|compile the source code of the project.|
|```process-classes```|post-process the generated files from compilation, for example to do bytecode enhancement on Java classes.|
|```generate-test-sources```|generate any test source code for inclusion in compilation.|
|```process-test-sources```|process the test source code, for example to filter any values.|
|```generate-test-resources```|create resources for testing.|
|```process-test-resources```|copy and process the resources into the test destination directory.|
|```test-compile```|compile the test source code into the test destination directory.|
|```process-test-classes```|post-process the generated files from test compilation, for example to do bytecode enhancement on Java classes.|
|```test```|run tests using a suitable unit testing framework. These tests should not require the code be packaged or deployed.|
|```prepare-package```|perform any operations necessary to prepare a package before the actual packaging. This often results in an unpacked, processed version of the package.|
|```package```|take the compiled code and package it in its distributable format, such as a JAR.|
|```pre-integration-test```|perform actions required before integration tests are executed. This may involve things such as setting up the required environment.|
|```integration-test```|process and deploy the package if necessary into an environment where integration tests can be run.|
|```post-integration-test```|perform actions required after integration tests have been executed. This may including cleaning up the environment.|
|```verify```|run any checks to verify the package is valid and meets quality criteria.|
|```install```|install the package into the local repository, for use as a dependency in other projects locally.|
|```deploy```|done in an integration or release environment, copies the final package to the remote repository for sharing with other developers and projects.|

**Table retrieved from official Maven webpage*

#### Site
The ```mvn site``` command generates a site from a Maven project. The site will consist of project documentation and reports about the project. The site lifecycle consists of four phases - **pre-site**, **site**, **post-site**, and **site-deploy**.
Reports generated by maven are included in the generated site.

### Maven POM
The Project Object Model, or POM, is an XML file that contains most, if not all of the information and configuration details needed to build the project.

The minimum requirements for a ```pom.xml``` file consists of the following nodes:-

|Node|Description|
|---|---|
|```<project>```|This is project root tag. You need to specify the basic schema settings such as apache schema and w3.org specification.|
|```<modelVersion>```|Model version should be 4.0.0|
|```<groupId>```|This is an Id of project's group. This is generally unique amongst an organization or a project. For example, a banking group com.company.bank has all bank related projects. In some  programming language the groupId is associated with namespaces|
|```<artifactId>```|This is an Id of the project. This is generally name of the project. For example, consumer-banking. Along with the groupId, the artifactId defines the artifact's location within the repository.|
|```<version>```|This is the version of the project. Along with the groupId, It is used within an artifact's repository to separate versions from each other.|

Some other notable extensions of the POM include configuring reports as well as build profiles. Reports can be added to web site to display the current state of the project. The way these reports are generated is by extracting information from the POM. This is done by adding the **Maven Project Info Reports Plugin** to the ```reporting``` element in the POM. In doing so, it is possible to obtain a number of standard reports regarding the project, including but not limited to:-
- Continous Integration
- Dependencies
- Issue Tracking
- License
- Mailing Lists
- Project Team
- Source Repository

Maven uses build profiles to create customized build configurations for a project. The main reason for this is to ensure portability in its builds. In order for builds to be protable, it is necessary to avoid all references to filesystems and leaning heavily towards the local repository to store the required metadata. Due to the fact that complete portability may not always be achievable, Maven resolves this issue by introducing customizable build profiles that can be modified to suite each and every scenario.

## Output of Maven

### Maven Artifacts
An artifact is a by-product of software development. It can be anything that is created so that a certain software can be developed. This includes but is not limited to data models, diagrams, setup scripts, and executable files. In thee context of Apache Maven, an artifact is the resulting output file of a Maven build that gets deployed into a Maven repository. The parameters of  ```groupId```, ```artifactId```, and ```version``` are used to identify the dependencies that are needed to build and run the code.

The Maven artifact should not be confused with a Maven ```Artifact```, which is a Java class that is dereferenced by a repository manager into a repository manager artifact. When used in this sense, an ```Artifact``` is used as a point of reference.

A Maven project may depend on several ```Artifact```s by way of its ```dependency``` elements. Maven interacts with a repository manager to resolve those ```Artifact```s into files by instructing the repository manager to send it some repository manager artifacts that correspond to the internal ```Artifact```s. Finally, after resolution, Maven builds the project and produces a Maven artifact which is deployed into the repository.

### Runnable Artifacts
Maven artifacts can be any sort of file, notably ```.jar```, ```.war```, ```.zip```, or ```.dll```. However, more often than not the output would be a runnable artifact, such as a ```.jar``` file.

### Sites Documentation
The ```maven-site-plugin``` is the main plugin that is used to crate all site documentation. The ```maven-project-info-report-plugin``` is the main plugin used by the ```maven-site-plugin``` to collate and produce the common site reporting docs for the artifact. The ```maven-project-info-report-plugin``` also generates a set of reports automatically which provide additional information on top of the redundant information provided by some common plugins.

List of automatically provided reports
- distribution-management
- index
- dependencies
- help
- issue-tracking
- plugins
- cim
- license
- dependency-management
- mailing-list
- project-team
- dependency-convergence
- scm
- plugin-management
- modules
- summary

## Maven Dependencies
The Maven dependency management system is a core function of Maven that helps in defining, creating, and maintaining reproducible builds with well-defined classpaths and library versions. It helps in centralizing dependency information, such as in the case of a set of projects that inherit from a common parent. In this scenario, it is possible to put all information about the dependency in the common POM and have simpler references to the artifacts in the child POMs, thus allowing managed dependencies to be specified through inheritance.

However, in larger projects it may not be possible to accomplish this since a project is only able to inherit dependencies from a single parent. To overcome this, a project may import managed dependencies from other projects by declaring a POM artifact as a dependency with a scope of ```import```. Imports are most effective when used to define a library of related artifacts that are generally part of a multiproject build.

It is fairly common for one project to use one or more artifacts from these libraries, thus it can be troublesome to keep the versions in the project using the artifacts in sync with the versions distributed in the library. This issue can be solved using a bill of materials (BOM) for use by other projects. The root of the project is the BOM POM. It defines the versions of all the artifacts that will be created in the library. Other projects that wish to use the library should import this POM into the ```dependencyManagement``` section of their POM.

System dependencies are always available and are not looked up in repository. They are declared using the scope ```system```. They are usually used to tell Maven about dependencies which are provided by the JDK or the VM, which makes them useful in resolving dependencies on artifacts which used to be seperate downloads earlier, but now available through the JDK.

#### Sources
- https://www.codetab.org/tutorial/apache-maven/
- https://maven.apache.org/guides/introduction/introduction-to-dependency-mechanism.html#